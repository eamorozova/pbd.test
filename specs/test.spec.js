const supertest = require("supertest");

test('API возвращает список станций', async () => {
    const { status } = await supertest('https://www.pobeda.aero')
        .get('/resources/api/v1/resource/stations')
        .set('apikey', '3570b7304cf0c3397670581657a6ee0ef72256bcb4c4021be666fd144c8d9f20')
    expect(status).toBe(200)
});
