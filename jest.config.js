module.exports = {
  clearMocks: true,
  moduleFileExtensions: ['js', 'json'],
  testMatch: ['**/specs/*.spec.*'],
  testEnvironment: "node",
};
